#!/usr/bin/env bash

############################################################
#Get Options
############################################################
Help(){
  echo "Help:"
  echo "  $0 -u url1 -u url2 -o <output-format>"
  echo ""
  echo "Options:"
  echo "  -u argument : set a single url with each argument"
  echo "  -o argument : choose output stdout/json"
  echo ""
  exit
}

while getopts "u:o:" option; do     # we state that u and o flags must be followed by a value
  case $option in
    "u")
      input+=("$OPTARG")            # we're making an array of all the inputed urls 
      echo "Url $OPTARG added to the list."
      ;;
    "o")
      output=$OPTARG                # storing output format
      echo "$OPTARG output selected."
      ;;
    *)                              # in case a wrong flag is inputed
      help
      ;;
  esac
done

if [ "$OPTIND" -eq "1" ] || [ "$OPTIND" -le "$#" ]; then    # returns help on wrong inputs
  help
fi

############################################################
# Parse url
############################################################
>output.json
>urls.txt 

for url in "${input[@]}"; do
  lynx -dump -listonly -nonumbers $url | grep '^http' >> urls.txt   # using lynx comand-line browser to extract url within the html 
done                                                                # stores it to urls.txt  


if [ "$output" = json ]; then                   # checking if we need to format to json
  echo "Working on json formating, couldn't make it work with bash";
  urls=$(sort urls.txt | uniq);                 # sorts inputed urls and removes duplicates
  for url in $urls; do
    domain=$(echo $url | cut -d/ -f-3);         # using cut to separate urls in groups (based on /)
    path=/+$(echo $url | cut -d/ -f4-);         # adding / at the beginning of the path string
    jq -n --arg do "$domain" --arg pa "$path" '.data + {domain:$do,path:$pa}' >> output.json; # adding lines to json
  done
  sed -i ':a;N;$!ba;s/\n//g' output.json          # removes new line
  sed -i 's/}{/},{/g' output.json                 # adds comma 
  sed -i '1s/^/[/' output.json                    # adds bracket at the beginning of the file
  echo ']' >> output.json                         # and at the end
  sed -i 's/ //g' output.json                     # removes blank space
  jq '.' < output.json                            # outputs the "json" 
else
  sort urls.txt | uniq                          # output sorted inputed urls after (without duplicates) 
fi

rm -f output.json                                  # deleting temporary file we used
rm -f urls.txt
sleep infinity