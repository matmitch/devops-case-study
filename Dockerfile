FROM bash
COPY ./main.sh /
RUN apk add --no-cache lynx
ENTRYPOINT [ "/main.sh" ]