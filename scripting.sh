#!/usr/bin/env bash

# Extracts base domain name from list: ./scripting.sh liste.exemple 
# Solution inspired from this website: https://www.appsloveworld.com/bash/100/4/how-to-extract-domain-name-from-url

>tmp1.txt
>tmp2.txt


while read url; do
  echo "$url" | sed -e 's#^https://\|^http://##' -e 's#:.*##' -e 's#/.*##' | rev | cut -d '.' -f -2 | rev | tr '[:upper:]' '[:lower:]' >> tmp1.txt
  echo "$url" | sed -E -e 's_.*://([^/@]*@)?([^/:]+).*_\2_' |  rev | cut -d '.' -f -2 | rev | tr '[:upper:]' '[:lower:]' >>tmp2.txt
done <$1



echo -e "\nFirst Method\n"
sort tmp1.txt | uniq                          # output sorted inputed urls after (without duplicates) 
echo " "
echo "----------------------------------"
echo -e "\nSecond Method\n"
sort tmp2.txt | uniq                          # output sorted inputed urls after (without duplicates) 
echo " "

rm tmp1.txt
rm tmp2.txt
